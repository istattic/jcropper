const mix = require('laravel-mix');

mix.sass('./src/sass/jcropper.scss', './dist/jcropper.css')
    .js('./src/js/jcropper.js', './dist/jcropper.js')
    .setPublicPath('dist');
